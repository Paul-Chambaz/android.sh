#!/bin/sh

# This script is an installation wizard for android.sh
# TODO: it is very minimal and would benefit of being more in depth

android_home="$HOME/dev/android"
scripts="$HOME/.scripts"
zsh_autocompletions="/usr/share/zsh/site-functions/_android.sh"

cp -f "android.sh" "$scripts/"
cp -f "_android.sh" "$zsh_autocompletions/"
cp -fr ".template" "$android_home/"
