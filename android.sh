#!/bin/bash

root="$HOME/dev/android"
green="\e[32;1m"
norm="\e[0m"

warn () {
  printf "$*\n" 2>&1
}

die () {
  printf "$*\n" 2>&1
  exit 1
}

help_fn () {
  bold=$(tput bold)
  norm=$(tput sgr0)

  printf "Usage ${bold}android.sh [command]${norm}\n"
  printf "Commands:\n"
  printf " ${bold}--init${norm}                          Creates a new android project, prompts to ask for a title\n"
  printf " ${bold}--list${norm}                          Returns a list of all android projects\n"
  printf " ${bold}--open [project_name]${norm}           Opens an android project in the editor\n"
  printf " ${bold}--run [project_name]${norm}            Runs an android project in emulator\n"
  printf " ${bold}--build [project_name]${norm}          Creates a signed package of an android project\n"
  printf " ${bold}--git [project_name]${norm}            Initializes a git repository for an android project\n"
  printf " ${bold}--install [project_name]${norm}        Installs an android project on a android smartphone using adb\n"
  printf " ${bold}--install_wifi [project_name]${norm}   Installs an android project on a android smartphone over wifi\n"
  printf " ${bold}--remove [project_name]${norm}         Removes an android project\n"
  printf " ${bold}--help${norm}                          Prints the help message\n"

}

init_fn () {
  printf "Enter project name (eg. template): "
  read name
  [ -z "$name" ] && die "Error, please enter a project name"
  printf "Enter package name (eg. xyz.chambaz.template): "
  read package_name
  [ -z "$package_name" ] && die "Error, please enter a package name"
  cd "$root"

  cp -r .template "$name"
  find "$name" \( -type d -name .git -prune \) -o -type f  -print0 | xargs -0 sed -i "s/xyz.chambaz.template/${package_name}/g"
  find "$name" \( -type d -name .git -prune \) -o -type f  -print0 | xargs -0 sed -i "s/TEMPLATE/${name}/g"

  pn1="$( echo $package_name | cut -d '.' -f 1 )"
  pn2="$( echo $package_name | cut -d '.' -f 2 )"
  pn3="$( echo $package_name | cut -d '.' -f 3 )"

  [ ! "$pn1" = "xyz" ] && mv "$name/app/java/xyz" "$name/app/java/$pn1"
  [ ! "$pn2" = "chambaz" ] && mv "$name/app/java/xyz/chambaz" "$name/app/java/xyz/$pn2"
  mv "$name/app/java/xyz/chambaz/template" "$name/app/java/$pn1/$pn2/$pn3"

  [ ! "$pn1" = "xyz" ] && mv "$name/app/src/androidTest/java/xyz" "$name/app/src/androidTest/java/$pn1"
  [ ! "$pn2" = "chambaz" ] && mv "$name/app/src/androidTest/java/xyz/chambaz" "$name/app/src/androidTest/java/xyz/$pn2"
  mv "$name/app/src/androidTest/java/xyz/chambaz/template" "$name/app/src/androidTest/java/$pn1/$pn2/$pn3"

  [ ! "$pn1" = "xyz" ] && mv "$name/app/src/main/java/xyz" "$name/app/src/main/java/$pn1"
  [ ! "$pn2" = "chambaz" ] && mv "$name/app/src/main/java/xyz/chambaz" "$name/app/src/main/java/xyz/$pn2"
  mv "$name/app/src/main/java/xyz/chambaz/template" "$name/app/src/main/java/$pn1/$pn2/$pn3"

  [ ! "$pn1" = "xyz" ] && mv "$name/app/src/test/java/xyz" "$name/app/src/test/java/$pn1"
  [ ! "$pn2" = "chambaz" ] && mv "$name/app/src/test/java/xyz/chambaz" "$name/app/src/test/java/xyz/$pn2"
  mv "$name/app/src/test/java/xyz/chambaz/template" "$name/app/src/test/java/$pn1/$pn2/$pn3"

  keytool -genkey -keystore "$HOME/.config/android/key_store.jks" -storepass "android" -keyalg RSA -keysize 2048 -validity 1000000 -alias "key_${name}" -dname "CN=Paul Chambaz"
  printf "storeFile=$HOME/.config/android/key_store.jks\n" >> "$name/keystore.properties"
}

list_fn () {
  ls "$root"
}

open_fn () {
  cd "$root"
  [ ! -d "$1" ] && die "Error, this project does not exist"
  cd "$root/$1/app/src/main/java"
  cd "$(ls)"
  cd "$(ls)"
  cd "$(ls)"
  $EDITOR MainActivity.kt
}

run_fn () {
  cd "$root"
  [ ! -d "$1" ] && die "Error, this project does not exist"
  cd "$root/$1"
  [ "$( { adb shell exit; } 2>&1)" = "adb: no devices/emulators found" ] || [ "$( { adb shell exit; } 2>&1)" = "adb: device offline" ] && /opt/android-sdk/emulator/emulator @android_32 &
  adb install "app/build/outputs/apk/release/app-release.apk"
  while [ ! "$(adb shell getprop init.svc.bootanim)" = "stopped" ]
  do
    sleep 1
  done
  package_name="$(cat app/manifests/AndroidManifest.xml | grep -o "package=\".*\"" | cut -d '"' -f 2)"
  adb shell monkey -p "$package_name" -c android.intent.category.LAUNCHER 1 2&>1
}

build_fn () {
  cd "$root"
  [ ! -d "$1" ] && die "Error, this project does not exist"
  cd "$root/$1"
  ./gradlew assembleRelease
}

git_fn () {
  cd "$root"
  [ ! -d "$1" ] && die "Error, this project does not exist"
  cd "$root/$1"
  git init -q
  git remote add gitlab "git@gitlab.com:Paul-Chambaz/$1.git"
  git add .
  git commit -m "Initial commit" -q
  git push --set-upstream gitlab master
}

install_fn () {
  cd "$root"
  [ ! -d "$1" ] && die "Error, this project does not exist"
  cd "$root/$1"
  ./gradlew assembleRelease
  adb -d install "app/build/outputs/apk/release/app-release.apk"
  package_name="$(cat app/manifests/AndroidManifest.xml | grep -o "package=\".*\"" | cut -d '"' -f 2)"
  adb shell monkey -p "$package_name" -c android.intent.category.LAUNCHER 1 2&>1
}

install_wifi_fn () {
  # TODO: this is very buggy - understand why that is and fix it
  # TODO: it would be poggers to do this with qr codes
  cd "$root"
  [ ! -d "$1" ] && die "Error, this project does not exist"
  cd "$root/$1"

  # what we want to do is first try then wizard
  if [ ! -z "$( { adb shell exit; } 2>&1)" ]
  then
    warn "Warning, could not connect to phone, attempting connection."
    printf "Navigate to <Settings/System/Developper options/Wireless debugging>\n"
    printf "Enter IP address & port (X.X.X.X:P): "
    read ip_address_port
    [ -z "$ip_address_port" ] && die "Error, please enter an ip address and port"
    adb connect $ip_address_port
    if [ ! -z "$( { adb shell exit; } 2>&1)" ]
    then
      warn "Warning, phone not paired, attempting paired."
      printf "Navigate to <Settings/System/Developper options/Wireless debugging/Pair device with pairing code>\n"
      printf "Enter ip address and port (X.X.X.X:P): "
      read ip_address_port_pair
      [ -z "$ip_address_port_pair" ] && die "Error, please enter an ip address and port"
      adb pair $ip_address_port_pair
      adb connect $ip_address_port
      [ ! -z "$( { adb shell exit; } 2>&1)" ] && die "Error, could not connect."
    fi
  fi
  ./gradlew assembleRelease
  adb install "app/build/outputs/apk/release/app-release.apk"
  package_name="$(cat app/manifests/AndroidManifest.xml | grep -o "package=\".*\"" | cut -d '"' -f 2)"
  adb shell monkey -p "$package_name" -c android.intent.category.LAUNCHER 1 2&>1

  # qrencode -s 32 "test" -o /tmp/qr && sxiv /tmp/qr && rm /tmp/qr
}

remove_fn  () {
  cd "$root"
  [ ! -d "$1" ] && die "Error, this project does not exist"
  rm -fr "$1"
  keytool -keystore "$HOME/.config/android/key_store.jks" -storepass "android" -delete -alias "key_$1"
}

[ $# -eq 0 ] && printf "Error, no arguments\n" && help_fn && exit 0

if [ "$1" = "--init" ]
then
  init_fn
elif [ "$1" = "--list" ]
then
  list_fn
elif [ "$1" = "--open" ]
then
  [ $# -eq 1 ] && die "Error, please enter a project name"
  open_fn "$2"
elif [ "$1" = "--run" ]
then
  [ $# -eq 1 ] && die "Error, please enter a project name"
  run_fn "$2"
elif [ "$1" = "--build" ]
then
  [ $# -eq 1 ] && die "Error, please enter a project name"
  build_fn "$2"
elif [ "$1" = "--install" ]
then
  [ $# -eq 1 ] && die "Error, please enter a project name"
  install_fn "$2"
elif [ "$1" = "--install_wifi" ]
then
  [ $# -eq 1 ] && die "Error, please enter a project name"
  install_wifi_fn "$2"
elif [ "$1" = "--git" ]
then
  [ $# -eq 1 ] && die "Error, please enter a project name"
  git_fn "$2"
elif [ "$1" = "--remove" ]
then
  [ $# -eq 1 ] && die "Error, please enter a project name"
  remove_fn  "$2"
else
  help_fn
fi
