# android.sh

## About

This project is a script used to replace android studio.
It does not aim to be a full replacement of android studio.
The goal is to be able to code using a minimalist text editor (eg. vim, emacs) and still be able to code android applications.
This script works for kotlin - though it would be very easy to expand it to java.

To use this project, you probably need a better understanding of android than what is necessary with android studio.
For example if you want to add a button, you do need to know what to add to the project and in which document.

Features include:
+ init - initializes a new android project
+ list - lists all android project
+ open - opens an android project
+ remove - removes an android project
+ build - builds an android project
+ run - runs the android project in an emulator
+ install - installs the android project on a phone with usb
+ install_wifi - installs the android project on a phone with wifi
+ git - initializes a git repo and send it remotely

There are also zsh autocompletions - I will add bash autocompletions if someone asks for it.

It is configured for me though you can probably make it work for you very easily.
In the future, I will add to the install.sh so that it becomes a fully featured installation wizard.

## Installation

To install this script, simply clone the repo and run the install.sh script.
(You can also view the content of install.sh and do things manually)

```
git clone https://gitlab.com/Paul-Chambaz/android.sh
cd android.sh
sh install.sh
```

## Licence

This project is licenced under the GPLv2 licence.
For more information, read the LICENSE file.
