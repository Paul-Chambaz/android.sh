#compdef android.sh

_android.sh() {

  local -a commands

  commands=(
    "--init:Creates a new android project, prompts to ask for a title"
    "--list:Returns a list of all android projects"
    "--open:Opens an android project in the editor"
    "--run:Runs an android project in emulator"
    "--build:Creates a signed package of an android project"
    "--git:Initializes a git repository for an android project"
    "--install:Installs an android project on a android smartphone using adb"
    "--install_wifi:Installs an android project on a android smartphone over wifi"
    "--remove:Removes an android project"
    "--help:Prints the help message"
  )

  _arguments -C \
    '1:cmd:->cmds' \
    '*:: :->args'

  case "$state" in
  cmds)
    _describe -t commands 'commands' commands
    ;;
  *)

    local android_projects=( ${(f)"$(ls $HOME/dev/android)"} )
    case $words[1] in
    --open)
      _describe -t output '' android_projects
      ;;
    --run)
      _describe -t output '' android_projects
      ;;
    --build)
      _describe -t output '' android_projects
      ;;
    --git)
      _describe -t output '' android_projects
      ;;
    --install)
      _describe -t output '' android_projects
      ;;
    --install_wifi)
      _describe -t output '' android_projects
      ;;
    --remove)
      _describe -t output '' android_projects
      ;;
    esac
    ;;
  esac
}

_android.sh

